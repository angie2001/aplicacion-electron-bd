//Librerias
const {ipcRenderer}=require("electron");
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
  } = require("./js/data");

  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");

//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{

    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        // evento.preventDefault();
        let inicioCuenta = {email: datos.email};
        todo(inicioCuenta);
    })
    
    const todo= async(inicioCuenta)=>{

        //MOSTRAR LAS CATEGORÍAS
        // let datos_categorias = await accesoCategorias();
        let datos_categorias = await obtenerCategorias();
        let arrCategoriasMostrar = [];
        console.log("estoy en mi menu " + inicioCuenta.email);
        for (const validacion of datos_categorias) {
        //for (const validacion of datos_categorias.categorias_cuentas) {
            if (validacion.email == inicioCuenta.email) {
                arrCategoriasMostrar.push(validacion.categoria);
                console.log(validacion.categoria);
            }
        }
        let categorias= document.getElementById("categorias");

        arrCategoriasMostrar.forEach(element => {
            let li = document.createElement('li');
            li.classList.toggle("n_categoria");
            let div = document.createElement('div');
            div.classList.toggle("n_nombre");
            div.innerHTML= `<input type='radio' class='form-check-input n_check' name='registro_cat' id ='${element}' >${element}`;
            li.appendChild(div);
            categorias.appendChild(li);
        });
        
        //FIXME:PARA CERRAR LA SESIÓN
        const iconoCerrarSesion = document.getElementById("iconoCerrarSesion");
        iconoCerrarSesion.addEventListener("click",async(evento)=>{
            alert(`SE CIERRA LA SESIÓN`);
            categorias.innerHTML = "";
            tGastos.innerHTML="";
            // datos_categorias = [];
            // datos_gastos = [];
            // inicioCuenta = {};
            alert(`DATOS GUARDADOS`);
        })
        //OPCIONES PARA CATEGORÍA
        const iconoAñadirCategoria = document.getElementById("iconoAñadirCategoria");
        iconoAñadirCategoria.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            ipcRenderer.send("añadirLaCategoria");
        })
        const iconoEditarCategoria = document.getElementById("iconoEditarCategoria");
        iconoEditarCategoria.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrCategoriasMostrar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
                
            })
            // alert(`Selección: ${seleccionada}`);

            ipcRenderer.send("editarLaCategoria",seleccionada);
        })
        const iconoEliminarCategoria = document.getElementById("iconoEliminarCategoria");
        iconoEliminarCategoria.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrCategoriasMostrar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
            })
            // alert(`Selección: ${seleccionada}`);

            ipcRenderer.send("eliminarLaCategoria",seleccionada);
        })
        //MOSTRAR GASTOS
        //let datos_gastos = await accesoGastos();
        let datos_gastos = await obtenerGastos();
        let arrGastosMostrar = [];
        console.log("estoy en mi menu " + inicioCuenta.email);
        for (const validacion of datos_gastos) {
        //for (const validacion of datos_gastos.gastos_cuentas) {
            if (validacion.email == inicioCuenta.email) {
                arrGastosMostrar.push(validacion);
                console.log(validacion);
            }
        }
        //FIXME: MIRAR PC LINA
        let contadorG=0;
        let tbody= document.getElementById("tBodyGastos");
        while (tbody.firstChild) {
            tbody.removeChild(tbody.firstChild);
        }
        arrGastosMostrar.forEach(element =>{
            let tr = document.createElement('tr');

            contadorG++;
            let td1 = document.createElement('td');
            td1.innerHTML = `<input type='radio' name='seleccion_gasto' id ='gasto${contadorG}'>`;
            console.log(contadorG);
            tr.appendChild(td1);

            let td2 = document.createElement('td')
            td2.innerHTML = element.fecha;
            tr.appendChild(td2);

            let td3 = document.createElement('td')
            td3.innerHTML = element.descripcion;
            tr.appendChild(td3);

            let td4 = document.createElement('td')
            td4.innerHTML = element.gasto;
            tr.appendChild(td4);

            let td5 = document.createElement('td')
            td5.innerHTML = element.categoria;
            tr.appendChild(td5);

            tr.classList.toggle("table-primary");

            
            tbody.appendChild(tr);
            
        })
        //OPCIONES PARA GASTO
        const iconoAñadirGasto = document.getElementById("iconoAñadirGasto");
        iconoAñadirGasto.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            ipcRenderer.send("añadirElGasto");
        })
        const iconoEditarGasto = document.getElementById("iconoEditarGasto");
        iconoEditarGasto.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            let contadorG=0;

            arrGastosMostrar.forEach(element=>{
                contadorG++;
                if (document.getElementById(`gasto${contadorG}`).checked === true){
                    seleccionada = element;
                    console.log();
                }
            })
            ipcRenderer.send("editarElGasto",seleccionada);
        })
        const iconoEliminarGasto = document.getElementById("iconoEliminarGasto");
        iconoEliminarGasto.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            let contadorG=0;

            arrGastosMostrar.forEach(element=>{
                contadorG++;
                if (document.getElementById(`gasto${contadorG}`).checked === true){
                    seleccionada = element;
                    console.log();
                }
            })
            ipcRenderer.send("eliminarElGasto",seleccionada);
        })
    
        //OPCIONES VER GASTO
        let select = document.getElementById("f_categoria");
        arrCategoriasMostrar.forEach(element => {
            let cat =document.createElement('option');
            cat.id=element;
            cat.innerHTML=element;
            select.appendChild(cat);
        });

        let arrBuscar = ["historico","mensual","categorico"];
        const aceptarVerGastos = document.getElementById("aceptarVerGastos");
        aceptarVerGastos.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let seleccionada = "";
            arrBuscar.forEach(element=>{
                if (document.getElementById(element).checked === true){
                    seleccionada = element;
                }
                
            })
            // alert(`Selección: ${seleccionada}`);
            if(seleccionada == "historico"){
                //nada
                console.log("historico");
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:"nada"
                }
                ipcRenderer.send("mostrarLosGastos",informacion);

            }else if (seleccionada == "mensual"){
                //algo
                let selectM = document.getElementById("n_mes").value;
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:selectM
                }
                ipcRenderer.send("mostrarLosGastos",informacion);
                
            }else if (seleccionada == "categorico"){
                console.log("categorico");
                let selectC = document.getElementById("f_categoria").value;
                      
                let informacion ={
                    seleccionada:seleccionada,
                    seleccionOp:selectC
                }
                ipcRenderer.send("mostrarLosGastos",informacion);
                
            }


            
        })
    }

})