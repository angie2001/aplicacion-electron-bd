//librerias necesarias
const fs = require("fs");
const path = require("path");
const bcrypt = require("bcrypt");
const numeroSaltos = 10;
const {obtenerCuentas,
  obtenerCuenta,
  crearCuenta,
  obtenerCategorias,
  obtenerCategoria,
  crearCategoria,
  obtenerGastos,
  obtenerGasto,
  crearGasto,
  eliminarCategoria,
  eliminarGasto,
  editarCategoria,
  editarGasto,} = require("./post-generales")




/**
 *
 * @param {object} cuenta es de donde se obtiene el password de la cuenta ingresada
 */
const codificarClave = async(cuenta) => {
  let nuevaClave;
  //Codifica la clave ingresada por el usuario según el numeroSaltos
  //numeroSaltos => el número de rondas utilizadas para cifrar un hash
  nuevaClave = bcrypt.hashSync(cuenta.password, numeroSaltos);

  return nuevaClave;
};

/**
 *
 * @param {object} datos Son las cuentas registradas en datos_cuentas_registro.json
 * @param {object} nuevaCuenta Es la nueva cuenta que se va a incluir en el datos_cuentas_registro.json
 * @returns Actualización de los registros del archivo datos_cuentas_registro.json
 */
const buscarCuenta = async (datos, nuevaCuenta)=>{
  let contador = 0;
  //Verifica si algún parámetro esta vacio (usuario,email y password)
  if (
    nuevaCuenta.usuario == "" ||
    nuevaCuenta.email == "" ||
    nuevaCuenta.password == ""
  ) {
    console.log("Usted no a insertado ningún caracter");
    return;
  } else {
    contador = 1;
  }

  const indice = datos.findIndex((b) => b.email === nuevaCuenta.email);

  if (indice === -1 && contador == 1) {
    //Se llama a la función codificarClave para codificar la clave
    nuevaCuenta.password = await codificarClave(nuevaCuenta);

    await crearCuenta(nuevaCuenta);

    console.log("-------Se creó una nueva cuenta---------");
    console.log("");
    return datos,indice;
  } else {
    console.log("Intente otro email");
    console.log("");
    return datos,indice;
  }
};

/**
 *
 * @param {object} datos Son las cuentas cargadas desde el archivo datos_cuentas_registro.json
 * @param {object} inicioCuenta Es la cuenta que digitó el usuario y con la que se realizará la verificación
 * de existencia en datos
 * @returns {Int}  Para poder acceder al menú de sesión
 */
const verificarExisteC = async(datos, inicioCuenta)=>{
  if(inicioCuenta.email == ""|| inicioCuenta.password == ""){
    return;
  } else{
    let existe = false;
    for( const validacion of datos){
      if(validacion.email == inicioCuenta.email){
        const match = await bcrypt.compare(
          inicioCuenta.password,
          validacion.password
        );
        if(match){
          existe = true;
        }
      }
    }
    if(existe == true){
      console.log("SE PUDO INICIAR SESIÓN");
    }else{
      console.log("DATOS INCORRECTOS");
    }
    return existe;
  }
};
/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Es la nueva categoría digitada por el usuario y la que se va a incluir en el archivo datos_categorias_registro.json
 * @returns {object} La actualización del arreglo del archivo datos_categorias_registro.json
 */
const buscarCategoria = async(inicioCuenta, datos_cat, nueva_cat)=>{
  let contador = 1;

  //Se crea un objeto que contiene el email y el nombre de la nueva categoria
  let categoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.nombre,
  };

  //Se valida si existe una categoría con el mismo nombre asociada al email
  for (const validacion of datos_cat) {
    if (categoriaParaAgregar.email == validacion.email) {
      if (categoriaParaAgregar.categoria == validacion.categoria) {
        console.log("Ya se tiene una categoría con este nombre");
        contador = 0;
      }
    }
  }
  if (contador == 1) {
    await crearCategoria(categoriaParaAgregar);

    console.log("Se creó una nueva categoria");
    return contador;
  } else {
    console.log("Intente otro nombre de categoría");
    return contador;
  }
  
  
};

/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Es el nuevo nombre que va a tener una categoría ya registrada
 * @returns {Int} Para verificar si existe la categoría asociada a la sesión inicida
 */
const buscarCategoriaEditar = async(inicioCuenta, datos_cat, nueva_cat) =>{
  let aceptoCambio = 1;
  //Se crea un objeto que contiene el email y el nombre de la categoría digitados por el usuario
  let CategoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.nombre,
  };

  //Se valida si existe una categoria asociada con el email
  console.log(nueva_cat.nombre);
  for (const validacion of datos_cat) {
    if (CategoriaParaAgregar.email == validacion.email) {
      if (CategoriaParaAgregar.categoria == validacion.categoria) {
        aceptoCambio = 0;
      }
    }
  }
  return aceptoCambio;
  
};

/**
 *
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} nueva_cat Nombre de la categoría que se va a eliminar
 * @returns {Int} Para verificar si la categoría a eliminar existe y está asociada con la sesión iniciada
 */
const buscarCategoriaEliminar = async(inicioCuenta, datos_cat, nueva_cat)=>{
  let aceptoCambio = 1;
  //Se crea un objeto que contiene el email y el nombre de la categoría digitados por el usuario
  let CategoriaParaAgregar = {
    email: inicioCuenta.email,
    categoria: nueva_cat.categoria,
  };
  //Se valida si existe una categoria asociada con el email
  for (const validacion of datos_cat.categorias_cuentas) {
    if (CategoriaParaAgregar.email == validacion.email) {
      if (CategoriaParaAgregar.categoria == validacion.categoria) {
        aceptoCambio = 0;
      }
    }
  }
  return aceptoCambio;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nuevoNombreCategoria El nuevo nombre de la categoría
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
const cambioNombreCategoriaRegistro = async(
  datos_gastos,
  datos_cat,
  inicioCuenta,
  nuevoNombreCategoria
) =>{
  //Se crean unas variables de control
  let contador = 1;
  let indice = -1;
  let idx = -1;
  let identificador = "";
  //Se verifica en que posición esta la categoria a editar
  for (const validacion of datos_cat) {
    indice = indice + 1;
    if (inicioCuenta.email == validacion.email) {
      if (validacion.categoria == nuevoNombreCategoria.categoria) {
        contador = 0;
        idx = indice;
        identificador = validacion._id
      }
    }
  }
  if (contador == 0 && idx > -1) {
    //Se cambia el nombre de la cateogoría en el archivo datos_categorias_registro.json
    await editarCategoria(identificador,nuevoNombreCategoria.nombre);
    //datos_cat.categorias_cuentas[idx].categoria = nuevoNombreCategoria.nombre;
    //Se cambia el nombre de la categoría que esta asociada con un gasto
    let identificador_gasto ="";
    for (const validacion of datos_gastos) {
      if (validacion.email == inicioCuenta.email) {
        if (validacion.categoria == nuevoNombreCategoria.categoria) {
          validacion.categoria = nuevoNombreCategoria.nombre;
          identificador_gasto = validacion._id;
          await editarGasto(identificador_gasto,validacion);
        }
      }
    }
    // 
  } else {
    console.log("No existe una categoría igual");
    return;
  }

};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} categoriaConGasto Contiene el parámetro categoría digitada por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @param {object} RegistroGasto Contiene los parámetros de el gasto,descripción y fecha digitados por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @returns {object} Los gastos actualizados del archivo datos_gastos_registro.json
 */
const gastoParaRegistrar1 = async (
  datos_gastos,
  inicioCuenta,
  categoriaConGasto,
  registroGasto
) =>{

  //Se crea un objeto que contenga email,categoría,gasto,descripción y fecha del nuevo gasto
  let paraAgregarGasto = {
    email: inicioCuenta.email,
    categoria: categoriaConGasto.nombre,
    gasto: registroGasto.gasto,
    descripcion: registroGasto.descripcion,
    fecha: registroGasto.fecha,
  };
  await crearGasto(paraAgregarGasto);

  console.log("Se agregó el gasto");
  console.log(
    "Categoría: " +
      paraAgregarGasto.categoria +
      "    " +
      "Gasto: " +
      paraAgregarGasto.gasto +
      "    " +
      "Descripción: " +
      paraAgregarGasto.descripcion +
      "    " +
      "Fecha: " +
      paraAgregarGasto.fecha
  );
  
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} RegistroGasto Contiene los parámetros de el gasto,descripción y fecha digitados por
 * el usuario para poder registrar un gasto asociado a una categoría
 * @returns {object} Los gastos actualizados del archivo datos_gastos_registro.json
 */
const gastoParaRegistrar2 = async(
  datos_gastos,
  inicioCuenta,
  registroGasto
) =>{
  //Se crea un objeto que contiene email,gasto,descripción y la fecha
  //este tiene por defecto la catetegoria = general, porque el gasto no tienen una categoria asociada
  let paraAgregarGasto = {
    email: inicioCuenta.email,
    categoria: "general",
    gasto: registroGasto.gasto,
    descripcion: registroGasto.descripcion,
    fecha: registroGasto.fecha,
  };

  await crearGasto(paraAgregarGasto);
};

/**
 * Función para eliminar categoría CON gastos
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nombredeCategoriaEliminar Nombre de la categoría que se quiere eliminar
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
const eliminaciondeCategoria1 = async(
  datos_cat,
  datos_gastos,
  inicioCuenta,
  nombredeCategoriaEliminar
)=> {
  
//Se crea una variable de control
  let contador = -1;
  //Se crea un objeto que contiene el email y la categoría
  let categoriaParaEliminar = {
    email: inicioCuenta.email,
    categoria: nombredeCategoriaEliminar.categoria,
  };
  for(const validacion of datos_cat){
    if (categoriaParaEliminar.email == validacion.email) {
      if (categoriaParaEliminar.categoria == validacion.categoria) {
        identificador = validacion._id;
        await eliminarCategoria(identificador);
      }
    }
  }
  for(const validacion of datos_gastos){
    if (categoriaParaEliminar.email == validacion.email) {
      if (categoriaParaEliminar.categoria == validacion.categoria) {
        identificador = validacion._id;
        await eliminarGasto(identificador);
      }
    }
  }
};

/**
 * Función para eliminar categoría SIN gastos
 * @param {object} datos_cat Son los datos de las categorías registradas en el archivo datos_categorias_registro.json
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {object} nombredeCategoriaEliminar Nombre de la categoría que se quiere eliminar
 * @returns {object,object} Los datos actualizados de la categoria y en los gastos
 */
const eliminaciondeCategoria2 = async (
  datos_cat,
  datos_gastos,
  inicioCuenta,
  nombredeCategoriaEliminar
)=> {

  //Se crea un objeto que tiene contenido email y categoría
  let categoriaParaEliminar = {
    email: inicioCuenta.email,
    categoria: nombredeCategoriaEliminar.categoria,
  };
  let identificador ="";
  //Se reasignan los gastos de esta categoría a "general"
  for (const validacion of datos_gastos) {
    if (categoriaParaEliminar.email == validacion.email) {
      if (categoriaParaEliminar.categoria == validacion.categoria) {
        validacion.categoria = "general";
        identificador = validacion._id;
        await editarGasto(identificador,validacion);
      }
    }
  }
  for(const validacion of datos_cat){
    if (categoriaParaEliminar.email == validacion.email) {
      if (categoriaParaEliminar.categoria == validacion.categoria) {
        identificador = validacion._id;
        await eliminarCategoria(identificador);
      }
    }
  }

};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {Object} capturaGasto Gasto seleccionado por el usuario para editar
 * @param {Object} paraEditarGasto Nombre del parámetro que se va a editar del gasto (categoría, gasto, descripción o fecha)
 * @param {Object} nuevarespuesta Contiene el nuevo valor del parámetro que se editó del gasto
 * @returns {Object} Actulización del archivo datos_gastos_registro.JSON
 */
//FIXME:
const buscarGastoEditar =async(
  datos_gastos
) =>{
  
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {Object} capturaGasto Gasto seleccionado por el usuario para eliminar
 * @returns {Object} Actulización del archivo datos_gastos_registro.JSON
 */
//FIXME
const buscarGastoEliminar = async (datos_gastos, capturaGasto)=>{
  
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 */
const vistaGeneral = async (datos_gastos, inicioCuenta)=> {
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  datos_gastos.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });

  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = datos_gastos.length - 1; i >= 0; i--) {
    alreves.push(datos_gastos[i]);
  }

  //Se imprime el arreglo inverso con los gastos asociados a la cuenta
  let muestra =[];
  let contador = -1;
  for(const validacion of alreves){
    if(validacion.email == inicioCuenta.email){
      contador++;
      muestra.push(validacion);
    }
  }

  return muestra;
  //return alreves;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {Object} respuestaFiltro Es el nombre de la categoría con que se quiere realizar el filtro de los gastos
 */
const vistaCategoria = async(datos_gastos, inicioCuenta, respuestaFiltro) =>{
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  datos_gastos.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });
  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = datos_gastos.length - 1; i >= 0; i--) {
    alreves.push(datos_gastos[i]);
  }
  //Se imprime el arreglo inverso con los gastos asociados a la cuenta
  console.log("El flitro "+respuestaFiltro.nombre);
  let arrverC = [];
  for (const validacion of alreves) {
    if (
      validacion.email == inicioCuenta.email &&
      validacion.categoria == respuestaFiltro.nombre
    ) {
      console.log(
        "Categoría: " +
          validacion.categoria +
          "    " +
          "Gasto: " +
          validacion.gasto +
          "    " +
          "Descripción: " +
          validacion.descripcion +
          "    " +
          "Fecha: " +
          validacion.fecha
      );
      arrverC.push({
        email:validacion.email,
        categoria:validacion.categoria,
        gasto:validacion.gasto,
        descripcion:validacion.descripcion,
        fecha:validacion.fecha

      })
    }
  }
  return arrverC;
};

/**
 *
 * @param {object} datos_gastos Son los datos de los gastos registradas en el archivo datos_gastos_registro.json
 * @param {object} inicioCuenta  Es la cuenta con la que se inició la sesión
 * @param {Object} respuestaFiltro Es el nombre del mes con que se quiere realizar el filtro de los gastos
 */

const vistaMes = async(datos_gastos, inicioCuenta, respuestaFiltro)=> {
  //Se crea un arreglo vacío donde estarán guardados los gasto asociados a la cuenta y que son iguales
  //al mes digitado por el usuario
  let arregloFechas = [];
  let comparacion;
  //Se realiza la compración
  //Comparación es una variable de tipo Date, la cual será utilizada para obtener el mes con el que se llenará arregloFechas[]
  if (respuestaFiltro.nombre == "enero") {
    comparacion = new Date("2000/01/01");
  } else if (respuestaFiltro.nombre == "febrero") {
    comparacion = new Date("2000/02/01");
  } else if (respuestaFiltro.nombre == "marzo") {
    comparacion = new Date("2000/03/01");
  } else if (respuestaFiltro.nombre == "abril") {
    comparacion = new Date("2000/04/01");
  } else if (respuestaFiltro.nombre == "mayo") {
    comparacion = new Date("2000/05/01");
  } else if (respuestaFiltro.nombre == "junio") {
    comparacion = new Date("2000/06/01");
  } else if (respuestaFiltro.nombre == "julio") {
    comparacion = new Date("2000/07/01");
  } else if (respuestaFiltro.nombre == "agosto") {
    comparacion = new Date("2000/08/01");
  } else if (respuestaFiltro.nombre == "septiembre") {
    comparacion = new Date("2000/09/01");
  } else if (respuestaFiltro.nombre == "octubre") {
    comparacion = new Date("2000/10/01");
  } else if (respuestaFiltro.nombre == "noviembre") {
    comparacion = new Date("2000/11/01");
  } else if (respuestaFiltro.nombre == "diciembre") {
    comparacion = new Date("2000/12/01");
  } else {
    console.log("No existe ese mes");
    return;
  }
  //Variable de control
  let elmes;
  //Llenar arregloFechas[]
  for (const validacion of datos_gastos) {
    elmes = new Date(validacion.fecha);
    if (validacion.email == inicioCuenta.email) {
      if (elmes.getMonth() + 1 == comparacion.getMonth() + 1) {
        arregloFechas.push(validacion);
      }
    }
  }
  //Se utiliza la funcion sort.() para organizar los datos de manera coronológica
  arregloFechas.sort(function (a, b) {
    //Se resta a y b, si el resultado es positivo a se mantiene en su lugar
    //Si el resultado es negativo, b cambia de posicón con a
    //Si es 0 se mantienen ambas posiciones
    //Esta línea ordena los gastos de el más antiguo al más reciente
    return new Date(a.fecha).getTime() - new Date(b.fecha).getTime();
  });
  //Se crea un arreglo donde se mostrará el contenido del arreglo datos_gastos.gastos_cuentas inverso
  //Para que el orden cronológico sea del gasto más reciente al más antiguo
  let alreves = [];
  //Se invierte el arreglo
  for (let i = arregloFechas.length - 1; i >= 0; i--) {
    alreves.push(arregloFechas[i]);
  }
  //Se imprimen los gastos sociados a la cuenta
  let arrverM = [];
  for (validacion of alreves) {
    console.log(
      "Categoría: " +
        validacion.categoria +
        "    " +
        "Gasto: " +
        validacion.gasto +
        "    " +
        "Descripción: " +
        validacion.descripcion +
        "    " +
        "Fecha: " +
        validacion.fecha
    );
    arrverM.push({
      email:validacion.email,
      categoria:validacion.categoria,
      gasto:validacion.gasto,
      descripcion:validacion.descripcion,
      fecha:validacion.fecha

    })
  }
  return arrverM;
};

// function capturaSesion(cuenta){
//   let inicioCuenta = cuenta.email;

//   return inicioCuenta
// }
/**
 * Funciones que son exportadas al archivo app.js
 */
module.exports = {
  buscarCuenta,
  verificarExisteC,
  buscarCategoria,
  buscarCategoriaEditar,
  cambioNombreCategoriaRegistro,
  gastoParaRegistrar1,
  gastoParaRegistrar2,
  buscarCategoriaEliminar,
  eliminaciondeCategoria1,
  eliminaciondeCategoria2,
  buscarGastoEditar,
  buscarGastoEliminar,
  vistaGeneral,
  vistaCategoria,
  vistaMes,
};
