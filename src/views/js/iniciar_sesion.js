//Librerias
const {ipcRenderer}=require("electron");
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
  } = require("./js/data");

  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");

let inicioCuenta = {email:""};
//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{
    const boton_aceptar_iniciar_sesion = document.getElementById("boton_aceptar_iniciar_sesion");
    boton_aceptar_iniciar_sesion.addEventListener("click",async(evento)=>{
        evento.preventDefault();
        const textoEmailInicio = document.getElementById("textoEmailInicio");
        const entrada1 = textoEmailInicio.value;
        const textoPasswordInicio = document.getElementById("textoPasswordInicio");
        const entrada2 = textoPasswordInicio.value;

        const inicioSesion = {
            email: entrada1,
            password: entrada2
        }
        let datos = await obtenerCuentas();
        let existe = await verificarExisteC(datos, inicioSesion);
        console.log(existe);
        if(existe == true){
            inicioCuenta.email = inicioSesion.email;
            ipcRenderer.send("inicioLaSesion", inicioSesion);

            alert(`BIENVENID@ ${entrada1}`);
        }else{
            alert(`DATOS INCORRECTOS`);

        }



    })
})

module.exports={
    inicioCuenta,
};