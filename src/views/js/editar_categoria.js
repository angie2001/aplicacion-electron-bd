const {ipcRenderer, ipcMain}=require("electron");
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");
  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");
  
document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('categoriaSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
        })
    })
    
    const todo = async(inicioCuenta, seleccionada)=>{
        const botonAceptarCambioCategoria = document.getElementById("botonAceptarCambioCategoria");
            botonAceptarCambioCategoria.addEventListener("click",async(evento)=>{
                evento.preventDefault();
                const textoCategoria = document.getElementById("textoCategoria");
                const entrada1 = textoCategoria.value;
                
                let datos_categorias = await obtenerCategorias();
                let datos_gastos = await obtenerGastos();
                let original = {categoria:seleccionada};
                let nueva_cat = {nombre: entrada1};
                
                let aceptoCambio = await buscarCategoriaEditar(
                    inicioCuenta,
                    datos_categorias,
                    nueva_cat
                    );
                    
                if (aceptoCambio == 1) {
                        //Se edita el nombre de la categoría
                    let nuevoNombreCategoria = {categoria:original.categoria,nombre: nueva_cat.nombre};
                    await cambioNombreCategoriaRegistro(
                        datos_gastos,
                        datos_categorias,
                        inicioCuenta,
                        nuevoNombreCategoria
                      );
                      ipcRenderer.send('nombreNuevaCategoria');
                } else {
                    //alert(`Ya se tiene una categoría con el nuevo nombre`);
                    console.log("Ya se tiene una categoría con el nuevo nombre");
                    console.log("Intente ingresar otro nombre");
                }
           
            })
    }
  })