const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");

document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('gastoSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
        })
    })
    
    const todo = async(inicioCuenta, seleccionada) =>{
        let gastoEliminar1 = document.getElementById('gastoEliminar1');
        let gastoEliminar2 = document.getElementById('gastoEliminar2');
        let gastoEliminar3 = document.getElementById('gastoEliminar3');
        let gastoEliminar4 = document.getElementById('gastoEliminar4');
        gastoEliminar1.textContent= ("Fecha: "+seleccionada.fecha);
        gastoEliminar2.textContent= ("Descripción: "+seleccionada.descripcion);
        gastoEliminar3.textContent= ("Monto: "+ seleccionada.gasto)
        gastoEliminar4.textContent= ("Categoría: "+seleccionada.categoria);
        console.log(seleccionada);

        
        const botonBorrarGasto = document.getElementById("botonBorrarGasto");
        botonBorrarGasto.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let datos_gastos = await obtenerGastos();   
            const radioSi = document.getElementById("radioSi");
            
            if(radioSi.checked){
                await eliminarGasto(seleccionada._id);
                ipcRenderer.send('nombreNuevaCategoria');
            }
            
            

        })
    }

})