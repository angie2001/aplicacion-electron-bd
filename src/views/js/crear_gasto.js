const {ipcRenderer, ipcMain}=require("electron");
const moment = require('moment');
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");

//ÁRBOL DOM
document.addEventListener("DOMContentLoaded",()=>{
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        // evento.preventDefault();
        //alert(`ffffffff ${datos.email}`);
        todo(datos)
        //return datos;
    });

    function sumarDias (fecha){
        fecha.setDate(fecha.getDate()+1);
        return fecha;
    }
    const todo= async(datos) =>{
        
        const botonAceptarGasto = document.getElementById("botonAceptarGasto");
        botonAceptarGasto.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            let inicioCuenta = {email: datos.email};

            const textoFecha = document.getElementById("textoFecha");
            const entrada1 = textoFecha.value;
            const textoDescripcion = document.getElementById("textoDescripcion");
            const entrada2 = textoDescripcion.value;
            const textoMonto = document.getElementById("textoMonto");
            const entrada3 = textoMonto.value;
            const textoCategoria = document.getElementById("textoCategoria");
            const entrada4 = textoCategoria.value;
            const opcionSi = document.getElementById("opcionSi");
            const opcionNo = document.getElementById("opcionNo");

            let registroGasto = {
              gasto: entrada3,
              descripcion: entrada2,
              fecha: entrada1
            }
            let datos_categorias = await obtenerCategorias();
            let datos_gastos = await obtenerGastos();
            let fechacambio = moment(entrada1).format('YYYY/MM/DD');
            //fechacambio = sumarDias(fechacambio);
            console.log(fechacambio);
            let f1 = Date.parse(fechacambio);
            if (isNaN(f1)) {
              console.log("La fecha ingresada no es valida");
            } else {
              //Se verifica el valor del gasto (que sea valor positivo y mayor a 0)
              registroGasto.gasto = parseInt(registroGasto.gasto);
              if (registroGasto.gasto > 0) {
                //let respuesta = await preguntaGasto();
                //Si quiere asociar un gasto a una categoría
                //respuesta.pregunta = respuesta.pregunta.toLowerCase();
                if (opcionSi.checked) {
                //    console.log("respondio si");

                  let categoriaConGasto = {nombre: entrada4};
                  let existeCategoria = await buscarCategoriaEditar(
                    inicioCuenta,
                    datos_categorias,
                    categoriaConGasto
                  );
                  //Se verifica que existe la categoria en esa cuenta
                  if (existeCategoria == 0) {
                    //Se registra el gasto con categoría
                    //ditada
                    registroGasto.fecha = fechacambio;
                    await gastoParaRegistrar1(
                      datos_gastos,
                      inicioCuenta,
                      categoriaConGasto,
                      registroGasto
                    );
                    alert(`Gasto registrado`);
                    ipcRenderer.send('nombreNuevaCategoria');
                  } else {
                    console.log("No existe esa categoria en la cuenta");
                    alert(`Categoría no registrada en la cuenta`);
                    console.log("Intente nuevamente ingresar el gasto");
                  }
                } else {
                  //Se registra el gasto en general
                  registroGasto.fecha = fechacambio;
                  await gastoParaRegistrar2(
                    datos_gastos,
                    inicioCuenta,
                    registroGasto
                  );
                  alert(`Gasto registrado`);
                  ipcRenderer.send('nombreNuevaCategoria');
                }
              } else {
                alert(`Valor de gasto no permitido, intente nuevamente`);
              }
            }

    
        })
    }
})