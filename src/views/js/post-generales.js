const axios = require("axios");

//--CUENTAS--
//Obtener todas las cuentas
const obtenerCuentas = async() =>{
    //Obtiene las tareas del backend
    const cuentas = await axios.get("http://localhost:3000/cuentas");

    //console.log(cuentas.data);
    return cuentas.data;
}
//Obtener una cuenta
const obtenerCuenta = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c28f89e30622c749e4ff0"
    const cuenta = await axios.get(`http://localhost:3000/cuentas/${identificador}`);

    console.log(cuenta.data);
}
//Crear una cuenta
const crearCuenta = async(nuevaCuenta) =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/cuentas",{
        usuario: nuevaCuenta.usuario,
        email: nuevaCuenta.email,
        password: nuevaCuenta.password
    });
    console.log(respuesta.data);
};



//--CATEGORÍAS--
//Obtener todas las categorías
const obtenerCategorias = async() =>{
    const categorias = await axios.get(`http://localhost:3000/categorias_cuentas`);
    console.log("estoy en post");

    return categorias.data;
}
//Obtener una categoría
const obtenerCategoria = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c66dea79d8451837fa9e4"
    const categoria = await axios.get(`http://localhost:3000/categorias_cuentas/${identificador}`);

    console.log(categoria.data);
}
//Crear una categoria
const crearCategoria = async(categoriaParaAgregar) =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/categorias_cuentas",{
        email: categoriaParaAgregar.email,
        categoria: categoriaParaAgregar.categoria
    });
    console.log(respuesta.data);
};
//Editar una categoría
const editarCategoria = async(identificador,nuevoNombreCategoria) =>{
    //Crea la cuenta en backend
    const respuesta = await axios.patch(`http://localhost:3000/categorias_cuentas/${identificador}`,{
        categoria: nuevoNombreCategoria
    });
    console.log(respuesta.data);
};
//Elimina una categoria
const eliminarCategoria = async(identificador) =>{
    //Elimina la tarea del backend
    const categoria = await axios.delete(`http://localhost:3000/categorias_cuentas/${identificador}`);

    console.log(categoria.data);
}


//--GASTOS--
//Obtener todos los gastos
const obtenerGastos = async() =>{
    const gasto = await axios.get(`http://localhost:3000/gastos_cuentas`);

    return gasto.data;
}
//Obtener un gasto
const obtenerGasto = async() =>{
    //Obtiene las tareas del backend
    const identificador = "619c3da80e682d4997132e48"
    const gasto = await axios.get(`http://localhost:3000/gastos_cuentas/${identificador}`);

    console.log(gasto.data);
}
//Crear una gasto
const crearGasto = async(paraAgregarGasto) =>{
    //Crea la cuenta en backend
    const respuesta = await axios.post("http://localhost:3000/gastos_cuentas",{
        email: paraAgregarGasto.email,
        categoria: paraAgregarGasto.categoria,
        gasto: paraAgregarGasto.gasto,
        descripcion: paraAgregarGasto.descripcion,
        fecha: paraAgregarGasto.fecha
    });
    console.log(respuesta.data);
};
//Editar un gasto
const editarGasto = async(identificador,edicionGasto) =>{
    //Crea la cuenta en backend
    const respuesta = await axios.patch(`http://localhost:3000/gastos_cuentas/${identificador}`,{
        categoria: edicionGasto.categoria,
        gasto: edicionGasto.gasto,
        descripcion: edicionGasto.descripcion,
        fecha: edicionGasto.fecha
    });
    console.log(respuesta.data);
};
//Eliminar un gasto
const eliminarGasto = async(identificador) =>{
    //Obtiene las tareas del backend
    const gasto = await axios.delete(`http://localhost:3000/gastos_cuentas/${identificador}`);

    console.log(gasto.data);
}


module.exports ={
    obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,
}