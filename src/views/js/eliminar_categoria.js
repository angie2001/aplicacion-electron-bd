const {ipcRenderer, ipcMain}=require("electron");
const {
    buscarCuenta,
    verificarExisteC,
    buscarCategoria,
    buscarCategoriaEditar,
    cambioNombreCategoriaRegistro,
    gastoParaRegistrar1,
    gastoParaRegistrar2,
    buscarCategoriaEliminar,
    eliminaciondeCategoria1,
    eliminaciondeCategoria2,
    buscarGastoEditar,
    buscarGastoEliminar,
    vistaGeneral,
    vistaCategoria,
    vistaMes,
    inicioCuenta,
  } = require("./js/data.js");

  const {obtenerCuentas,
    obtenerCuenta,
    crearCuenta,
    obtenerCategorias,
    obtenerCategoria,
    crearCategoria,
    obtenerGastos,
    obtenerGasto,
    crearGasto,
    eliminarCategoria,
    eliminarGasto,
    editarCategoria,
    editarGasto,} =  require("./js/post-generales.js");

  document.addEventListener("DOMContentLoaded",()=>{
    let inicioCuenta={email:""};
    ipcRenderer.on('emailCuenta',(evento,datos)=>{
        inicioCuenta= {email: datos.email};
        ipcRenderer.on('categoriaSeleccionada',(evento1,seleccionada)=>{
            todo(inicioCuenta, seleccionada);
            // alert(`cuenta: ${inicioCuenta.email} Selección: ${seleccionada}`);

        })
    })
    const todo = async(inicioCuenta,seleccionada)=>{
        let catEliminar = document.getElementById('catEliminar');
        catEliminar.textContent=seleccionada; 

        //AHORA
        const botonBorrarCategoria = document.getElementById("botonBorrarCategoria");
        botonBorrarCategoria.addEventListener("click",async(evento)=>{
            evento.preventDefault();
            const radioNo = document.getElementById("radioNo");
            const radioSi = document.getElementById("radioSi");

            let datos_categorias =await obtenerCategorias();
            const datos_gastos =await obtenerGastos();
            const nombredeCategoriaEliminar = {categoria:seleccionada}
            if(radioNo.checked){
                // alert(`no`);
                await eliminaciondeCategoria2(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               );
               ipcRenderer.send('nombreNuevaCategoria');

            }else if(radioSi.checked){
                // alert(`si`);
                datos_gastos,
               (datos_categorias = await eliminaciondeCategoria1(
                 datos_categorias,
                 datos_gastos,
                 inicioCuenta,
                 nombredeCategoriaEliminar
               ));
               ipcRenderer.send('nombreNuevaCategoria');
            }

        })

    }



  })